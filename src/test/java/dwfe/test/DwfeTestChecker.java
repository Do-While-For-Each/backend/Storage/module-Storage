package dwfe.test;

import java.util.Map;

public class DwfeTestChecker
{
  public Boolean expectedResult;
  public int expectedStatus;
  public Map<String, Object> requestMap;
  public String expectedError;
  public Map<String, Object> expectedResponseMap;

  public static DwfeTestChecker of(Boolean expectedResult, int expectedStatus,
                                   Map<String, Object> requestMap,
                                   String expectedError)
  {
    var checker = new DwfeTestChecker();
    checker.expectedResult = expectedResult;
    checker.expectedStatus = expectedStatus;
    checker.requestMap = requestMap;
    checker.expectedError = expectedError;
    return checker;
  }

  public static DwfeTestChecker of(Boolean expectedResult, int expectedStatus,
                                   Map<String, Object> requestMap)
  {
    var checker = new DwfeTestChecker();
    checker.expectedResult = expectedResult;
    checker.expectedStatus = expectedStatus;
    checker.requestMap = requestMap;
    return checker;
  }

  public static DwfeTestChecker of(Boolean expectedResult, int expectedStatus,
                                   Map<String, Object> req,
                                   Map<String, Object> expectedResponseMap)
  {
    var checker = new DwfeTestChecker();
    checker.expectedResult = expectedResult;
    checker.expectedStatus = expectedStatus;
    checker.requestMap = req;
    checker.expectedResponseMap = expectedResponseMap;
    return checker;
  }
}
